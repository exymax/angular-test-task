import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { APP_CHANGE, increase, decrease } from '../actions/app.actions';

@Injectable()
export default class AppEffects {
  change$ = createEffect(() => this.actions$.pipe(
    ofType(APP_CHANGE),
    switchMap(() => [
      increase(),
      decrease(),
      decrease()
    ])
  ));

  constructor(private actions$: Actions) {}
}
