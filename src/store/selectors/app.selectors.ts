import { AppState } from '../index';

export const selectIncrementValue = (state: AppState) => state.app.incrementValue;
export const selectDecrementValue = (state: AppState) => state.app.decrementValue;
