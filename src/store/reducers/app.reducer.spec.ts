import appReducer, { initialState as originalInitialState, State } from './app.reducer';
import * as appActions from '../actions/app.actions';

describe('App Reducer', () => {
  const initialState = {...originalInitialState};

  it('should increase the first value by one', () => {
    expect(appReducer(initialState, appActions.increase()).incrementValue).toBe(-4);
  });

  it('should decrease the second value by one', () => {
    expect(appReducer(initialState, appActions.decrease()).decrementValue).toBe(9);
  });

  it('should reset current state to initial state', () => {
    // Values obviously different than in initial state
    const currentState: State = {
      incrementValue: 100,
      decrementValue: -200
    };

    const resetResultState: State = appReducer(currentState, appActions.reset);

    expect(resetResultState).toEqual(initialState);
  });
});
