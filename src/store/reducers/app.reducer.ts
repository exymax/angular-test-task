import { createReducer, on } from '@ngrx/store';
import * as appActions from '../actions/app.actions';

export interface State {
  incrementValue: number;
  decrementValue: number;
}

export const initialState: State = {
  incrementValue: -5,
  decrementValue: 10
};

export default createReducer(
  initialState,
  on(appActions.increase, (state) => ({ ...state, incrementValue: state.incrementValue + 1 })),
  on(appActions.decrease, (state) => ({ ...state, decrementValue: state.decrementValue - 1 })),
  on(appActions.reset, () => ({...initialState}))
);
