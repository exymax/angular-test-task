import appReducer, { State } from './reducers/app.reducer';

export interface AppState {
  app: State;
}

export const commonReducer = {
  app: appReducer
};
