import { createAction } from '@ngrx/store';

export const APP_CHANGE = '[APP] Change';

const APP_INCREASE = '[APP] Increase';
const APP_DECREASE = '[APP] Decrease';
const APP_RESET = '[APP] Reset';

export const change = createAction(APP_CHANGE);
export const increase = createAction(APP_INCREASE);
export const decrease = createAction(APP_DECREASE);
export const reset = createAction(APP_RESET);
