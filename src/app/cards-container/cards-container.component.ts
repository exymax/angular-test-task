import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as appSelectors from '../../store/selectors/app.selectors';
import { AppState } from '../../store';

@Component({
  selector: 'app-cards-container',
  templateUrl: './cards-container.component.html'
})
export class CardsContainerComponent implements OnInit {
  private incrementValue$: Observable<number>;
  private decrementValue$: Observable<number>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.incrementValue$ = this.store.select(appSelectors.selectIncrementValue);
    this.decrementValue$ = this.store.select(appSelectors.selectDecrementValue);
  }
}
