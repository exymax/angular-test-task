import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async, ComponentFixture, inject } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { AppComponent } from './app.component';
import { TestStore } from '../store/test-store';
import { AppState } from '../store';
import { change, reset } from '../store/actions/app.actions';

// Delay before the first `change` action dispatch after button is clicked
const CHANGE_ACTION_EMIT_DELAY = 1000;
// Number of times which `change` action should be called before the `Stop` button is clicked
const CHANGE_ACTION_CALL_TIMES = 3;

const getAppButtons = (compiledNativeElement: HTMLElement): NodeListOf<HTMLButtonElement> =>
  compiledNativeElement.querySelectorAll('.app-buttons button');

const getAppButtonAtIndex = (compiledNativeElement: HTMLElement, index: number): HTMLButtonElement =>
  getAppButtons(compiledNativeElement)[index];

const getAppButtonContent = (button: HTMLButtonElement) =>
  button.textContent.trim();

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let compiled: HTMLElement;
  let store: TestStore<AppState>;
  let dispatchSpy: jasmine.Spy;

  let startButton: HTMLButtonElement;
  let stopButton: HTMLButtonElement;
  let resetButton: HTMLButtonElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{
        provide: Store,
        useClass: TestStore
      }]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    startButton = getAppButtonAtIndex(compiled, 0);
    stopButton = getAppButtonAtIndex(compiled, 1);
    resetButton = getAppButtonAtIndex(compiled, 2);
  }));

  beforeEach(inject([Store], (testStore: TestStore<AppState>) => {
    store = testStore;
    dispatchSpy = spyOn(store, 'dispatch');
  }));

  it('should create the app', () => expect(app).toBeTruthy());

  it('should render app-cards-container component inside', () => {
    expect(compiled.querySelector('app-cards-container')).toBeTruthy();
  });

  it('should render div.app-buttons', () => {
    expect(compiled.querySelector('.app-buttons')).toBeTruthy();
  });

  it('should have `Start` button as the first button inside div.app-buttons', () => {
    expect(getAppButtonContent(startButton)).toBe('Start');
  });

  it('should have `Stop` button as the second button inside div.app-buttons', () => {
    expect(getAppButtonContent(stopButton)).toBe('Stop');
  });

  it('should have `Reset` button as the third button inside div.app-buttons', () => {
    expect(getAppButtonContent(resetButton)).toBe('Reset');
  });

  it('should dispath `change` action every second on the `Start` and until `Stop` buttons were clicked', (done) => {
    startButton.click();

    // Click the `Stop` button one rxjs timer tick earlier than the setTimeout done below
    // to ensure in stops `change` action emits
    setTimeout(() => stopButton.click(), CHANGE_ACTION_EMIT_DELAY * CHANGE_ACTION_CALL_TIMES);

    // Delay expect assumptions to one rxjs timer tick farther to ensure that `change` action
    // has been called exactly CHANGE_ACTION_CALL_TIMES times
    // after the `Start` and before the `Stop` buttons were clicked
    setTimeout(() => {
      expect(dispatchSpy).toHaveBeenCalledWith(change());
      expect(dispatchSpy).toHaveBeenCalledTimes(CHANGE_ACTION_CALL_TIMES);
      done();
    }, CHANGE_ACTION_EMIT_DELAY * (CHANGE_ACTION_CALL_TIMES + 1));
  });

  it('should dispatch `reset` action on `Reset` button click', () => {
    resetButton.click();
    expect(dispatchSpy).toHaveBeenCalledWith(reset());
  });
});
