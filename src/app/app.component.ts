import { Component, ViewChild, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { interval, fromEvent, Subscription } from 'rxjs';
import { takeUntil, concatMap, repeat } from 'rxjs/operators';
import { AppState } from '../store';
import { change, reset } from '../store/actions/app.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnDestroy {
  @ViewChild('startButton', {static: false}) startButton: ElementRef<HTMLButtonElement>;
  @ViewChild('stopButton', {static: false}) stopButton: ElementRef<HTMLButtonElement>;

  private changeActionSubscription: Subscription;

  constructor(private store: Store<AppState>) {}

  private onResetButtonClick() {
    this.store.dispatch(reset());
  }

  ngAfterViewInit() {
    const startButtonClickObservable = fromEvent(this.startButton.nativeElement, 'click');
    const stopButtonClickObservable = fromEvent(this.stopButton.nativeElement, 'click');

    this.changeActionSubscription = startButtonClickObservable.pipe(
      concatMap(() => interval(1000)),
      takeUntil(stopButtonClickObservable),
      repeat()
    ).subscribe(() => this.store.dispatch(change()));
  }

  ngOnDestroy() {
    if (this.changeActionSubscription) {
      this.changeActionSubscription.unsubscribe();
    }
  }
}
