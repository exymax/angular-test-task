import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CardComponent } from './card.component';

const POSITIVE_VALUE = 5;
const NEGATIVE_VALUE = -4;

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;
  let compiledComponentDiv: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    compiledComponentDiv = fixture.debugElement.nativeElement.querySelector('.card');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain provided input', () => {
    component.value = POSITIVE_VALUE;
    fixture.detectChanges();
    expect(compiledComponentDiv.innerText.trim()).toBe(POSITIVE_VALUE.toString());
  });

  it('should have `negative` class for negative number input', () => {
    component.value = NEGATIVE_VALUE;
    fixture.detectChanges();
    expect(compiledComponentDiv.classList.contains('negative')).toBe(true);
  });

  it('should NOT have `negative` class for positive number input', () => {
    component.value = POSITIVE_VALUE;
    fixture.detectChanges();
    expect(compiledComponentDiv.classList.contains('negative')).toBe(false);
  });
});
